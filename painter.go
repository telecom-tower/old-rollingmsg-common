// Copyright 2016 Jacques Supcik, HEIA-FR
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// 2015-07-29 | JS | First version
// 2016-12-11 | JS | Current release

package msgroller

import log "github.com/Sirupsen/logrus"

type Renderer interface {
	SetBitmap([]uint32)
	Render() error
	Wait() error
}

func Painter(input chan []uint32, ren Renderer, rows int, columns int) chan struct{} {
	output := make(chan struct{})
	go func() {
		defer close(output)
		for req := range input {
			if len(req) == rows*columns {
				ren.SetBitmap(req)
				ren.Render()
				ren.Wait()
			} else {
				log.Fatalf("Invalid frame (%d instead of %d)", len(req), rows*columns)
			}
		}
	}()
	return output
}
