// Copyright 2016 Jacques Supcik, HEIA-FR
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// 2015-07-29 | JS | First version
// 2016-12-11 | JS | Current release

package msgroller

import (
	"time"

	"github.com/BlueMasters/firebasedb"
	log "github.com/Sirupsen/logrus"
	"gitlab.com/telecom-tower/pixtrix"
)

type BitmapMessage struct {
	Matrix     *pixtrix.Pixtrix
	Preamble   int
	Checkpoint int
}

func Feeder(ref *firebasedb.Reference, closing chan chan error) chan *BitmapMessage {
	output := make(chan *BitmapMessage)

	go func() {
		defer close(output)
		for {
			s, err := ref.Subscribe()
			if err != nil {
				log.Error(err)
				time.Sleep(1 * time.Second)
				errc, ok := <-closing
				if ok {
					errc <- nil
					return
				}
				continue
			}

		loop:
			for {
				select {
				case errc := <-closing:
					s.Close()
					errc <- nil
					return
				case e := <-s.Events():
					if e == nil {
						break loop
					}
					log.Debugln("New event")
					if e.Type == "put" {
						log.Infoln("Message received")
						var msg BitmapMessage
						_, err := e.Value(&msg)
						if err != nil {
							log.Errorf("Error decoding event: %v", err)
						} else {
							// TODO: Check if msg is new.
							output <- &msg
						}
					}
				}
			}
			log.Printf("Stream closed. Re-opening...")
			s.Close()
		}
	}()
	return output
}
