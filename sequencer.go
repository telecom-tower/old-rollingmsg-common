// Copyright 2016 Jacques Supcik, HEIA-FR
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// 2015-07-29 | JS | First version
// 2016-12-11 | JS | Current release

package msgroller

type StripesMessage struct {
	stripes    [][]uint32
	preamble   int
	checkpoint int
}

func Sequencer(input chan *BitmapMessage, rows int, columns int) chan []uint32 {
	output := make(chan []uint32, 2)
	var currentMessage StripesMessage
	var nextRoll chan int

	roll := func(message StripesMessage, low, high int) {
		for i := low; i < high; i++ {
			output <- message.stripes[i%2][i*rows : (i+columns)*rows]
		}
	}

	go func() {
		defer close(output)
		for { // Loop forever
			select {
			case m := <-input:
				if m == nil {
					return
				}
				currentMessage = StripesMessage{
					m.Matrix.InterleavedStripes(),
					m.Preamble,
					m.Checkpoint,
				}
				nextRoll = make(chan int, 1)
				nextRoll <- 0
				// Display the message at least once
				roll(currentMessage, 0, currentMessage.checkpoint)
			case r := <-nextRoll:
				roll(
					currentMessage,
					currentMessage.checkpoint,                   // from checkpoint
					len(currentMessage.stripes[0])/rows-columns) // to the last position
				roll(currentMessage,
					currentMessage.preamble,   // from the preamble
					currentMessage.checkpoint) // to the checkpoint
				nextRoll <- r
			}
		}
	}()

	return output
}
